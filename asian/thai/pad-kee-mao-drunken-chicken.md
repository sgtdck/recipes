# Pad Kee Mao aka "Drunken Chicken".

## Ingredients

* 2 tablespoon cooking oil
* 2/3 lb. (300g) boneless chicken, cut into strips
* 1 teaspoon minced garlic
* 6 fresh Thai chilies peppers, cut lengthwise finely
* 1 1/2 tablespoon Thai fish sauce
* 1 1/2 teaspoon sweet soy sauce
* 1/2 teaspoon sugar
* 20 sweet basil leaves
* 1/3 cup water
* Cooked Rice

## Preparation

Heat the oil in a wok until almost smoking. Add the garlic and chilli peppers and stir-fry quickly until fragrant, about 30 seconds. Add chicken and stir-fry until no longer pink.
Add fish sauce, sweet soy sauce and sugar and stir to mix. Add basil leaves, stock or water and bring to boil. Serve over rice.

I also like to put fried eggs on top of the finished dish.

## Source

[](http://www.seriouseats.com/talk/2012/09/what-to-do-with-an-abundance-of-thai-basil.html)
